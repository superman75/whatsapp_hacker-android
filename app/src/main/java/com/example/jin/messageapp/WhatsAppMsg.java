package com.example.jin.messageapp;

public class WhatsAppMsg {

    public String id;
    public String phoneNumber;
    public String message;
    public Boolean isIncoming;
    public long time;

    public WhatsAppMsg(String id, String phoneNumber, String message, Boolean isIncoming, long time) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.isIncoming=isIncoming;
        this.time=time;
    }
}
