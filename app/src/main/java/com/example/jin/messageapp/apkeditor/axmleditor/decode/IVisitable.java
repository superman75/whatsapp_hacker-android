package com.example.jin.messageapp.apkeditor.axmleditor.decode;

public interface IVisitable {
	public void accept(IVisitor v);  
}
