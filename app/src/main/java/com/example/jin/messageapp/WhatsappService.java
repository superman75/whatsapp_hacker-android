package com.example.jin.messageapp;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by Jin on 4/16/2018.
 */

public class WhatsappService extends Service {

    private SMSListener smsListener = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("Service","Start");
        // Create an IntentFilter instance.
        IntentFilter intentFilter = new IntentFilter();

        // Add network connectivity change action.
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_SENT");
        // Set broadcast receiver priority.
        intentFilter.setPriority(100);

        // Create a network change broadcast receiver.
        smsListener = new SMSListener();
        registerReceiver(smsListener, intentFilter);

        // Register the broadcast receiver with the intent filter object.

        WhatsappMessage whatsappmessage = new WhatsappMessage();
        whatsappmessage.Ready(this);

        Log.e(SMSListener.MESSAGE_TAG, "onCreate: SMSListener is registered.");
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister screenOnOffReceiver when destroy.
        if(smsListener!=null)
        {
            unregisterReceiver(smsListener);
            Log.e(SMSListener.MESSAGE_TAG, "onDestroy: SMSListener is unregistered.");
        }
    }

}
